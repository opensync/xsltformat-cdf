/*
 * Copyright (C) 2009 Daniel Gollub <gollub@b1-systems.de>
 * Copyright (C) 2009 Instituto Nokia de Tecnologia
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 */

/**
 * @file   xsltformat.c
 * @author Adenilson Cavalcanti da Silva <adenilson.silva@indt.org.br>
 *
 * @brief  A generic xslt format plugin.
 *
 * \todo:
 * - add more error handling code
 * - test the plugin for real
 * - add support for other data types (notes, events, etc)
 * - utests?
 *
 */

#include <opensync/opensync.h>
#include <opensync/opensync-format.h>
#include "xslt_aux.h"

static osync_bool conv_xslt_to_contact(char *input, unsigned int inpsize, char **output, unsigned int *outpsize, osync_bool *free_input, const char *config, void *userdata, OSyncError **error)
{
	osync_bool result = FALSE;
	struct xslt_resources *converter = NULL;

	converter = (struct xslt_resources *)userdata;
	if ((result = xslt_transform(converter, input)))
		goto exit;

	if (!(*output = strdup((char*)converter->xml_str)))
		goto exit;

	*free_input = TRUE;

	/* TODO:
	 * - set OsyncError
	 * - use a common function to call xslt transform?
	 */

exit:
	return result;
}

static osync_bool conv_contact_to_xslt(char *input, unsigned int inpsize, char **output, unsigned int *outpsize, osync_bool *free_input, const char *config, void *userdata, OSyncError **error)
{
	osync_bool result = FALSE;
	struct xslt_resources *converter = NULL;

	converter = (struct xslt_resources *)userdata;
	if ((result = xslt_transform(converter, input)))
		goto exit;

	if (!(*output = strdup((char*)converter->xml_str)))
		goto exit;

	*free_input = TRUE;

	/* TODO:
	 * - set OsyncError
	 * - use a common function to call xslt transform?
	 */

exit:
	return result;
}


/*
static void destroy_format1(char *input, unsigned int inpsize, void *user_data)
{
	//
	// Here you have to free the data allocated by your format
	//
}
*/

osync_bool get_format_info(OSyncFormatEnv *env, OSyncError **error)
{
	OSyncObjFormat *format = osync_objformat_new("xslt", "contact", error);
	if (!format)
		return FALSE;

	if( !osync_format_env_register_objformat(env, format, error) )
		return FALSE;
	osync_objformat_unref(format);

	return TRUE;
}

void *initialize_xslt(const char* config, OSyncError **error)
{
	struct xslt_resources *converter = xslt_new();
	int result = 0;
	if (!converter)
		osync_error_set(error, OSYNC_ERROR_GENERIC,
				"Unable create xslt converter context.");
	if ((result = xslt_initialize(converter, config)))
		osync_error_set(error, OSYNC_ERROR_GENERIC,
				"Unable load xslt stylesheet.");

	return converter;

}

static osync_bool finalize_xslt(void *userdata, OSyncError **error)
{
	struct xslt_resources *converter = NULL;
	if (!userdata)
		return FALSE;

	converter = (struct xslt_resources *)userdata;
	xslt_delete(converter);
	return TRUE;
}

static osync_bool reg_conv(OSyncFormatEnv *env,
			   OSyncObjFormat *from, OSyncObjFormat *to,
			   OSyncFormatConvertFunc convert_func,
			   OSyncFormatConverterInitializeFunc initialize_func,
			   OSyncFormatConverterFinalizeFunc finalize_func)
{
	OSyncError *error = NULL;
	OSyncFormatConverter *conv = osync_converter_new(OSYNC_CONVERTER_CONV,
							 from, to,
							 convert_func, &error);
	if (!conv)
		goto error;

	osync_converter_set_initialize_func(conv, initialize_func);
	osync_converter_set_finalize_func(conv, finalize_func);
	if( !osync_format_env_register_converter(env, conv, &error) )
		goto error;
	osync_converter_unref(conv);

	return TRUE;

error:
	if( conv )
		osync_converter_unref(conv);
	osync_trace(TRACE_INTERNAL, "%s", osync_error_print(&error));
	osync_error_unref(&error);
	return FALSE;
}

osync_bool get_conversion_info(OSyncFormatEnv *env, OSyncError **error)
{
	osync_bool result;
	/* Supported formats */
	OSyncObjFormat *xslt_contact = osync_format_env_find_objformat(env, "xslt-contact");
	if (!xslt_contact) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Unable to find xslt-contact"
				" format");
		return FALSE;
	}

	OSyncObjFormat *xml_contact = osync_format_env_find_objformat(env, "xmlformat-contact");
	if (!xml_contact) {
		osync_error_set(error, OSYNC_ERROR_GENERIC, "Unable to find xmlformat-contact format");
		return FALSE;
	}

	/* XSLT to xml-contact */
	result = reg_conv(env, xslt_contact, xml_contact,
			  conv_xslt_to_contact,
			  initialize_xslt, finalize_xslt);
	if (!result)
		return FALSE;

	/* xml-contact to XSLT */
	result = reg_conv(env, xml_contact, xslt_contact,
			  conv_contact_to_xslt,
			  initialize_xslt, finalize_xslt);
	return result;

}

int get_version(void)
{
	/* always return 1 */
	return 1;
}
